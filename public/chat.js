// make connection
var socket = io.connect('http://10.0.0.139:8000');

// Query DOM
var message = document.getElementById('message'),
    handle = document.getElementById('handle'),
    btn = document.getElementById('send'),
    output = document.getElementById('output'),
    feedback = document.getElementById('feedback');

// Emit events
btn.addEventListener('click', function(){
    var obj = {
        message: message.value,
        handle: handle.value
    };
    var jsonObj= JSON.stringify(obj);

    socket.emit('chat', jsonObj);
    message.value = "";
});

message.addEventListener('keypress', function (event) {
    if(event.key === 'Enter')
        btn.click();
    socket.emit('typing', handle.value);
});

// Listen for events
socket.on('chat', function(data){
    console.log(typeof data);
    feedback.innerHTML = "";
    output.innerHTML += '<p><strong>' + data.handle + ': </strong>' + data.message + '</p>';
});

socket.on('typing', function (data) {
    feedback.innerHTML = '<p><em>' + data + ' mesaj yazıyor...</em></p>';
});
