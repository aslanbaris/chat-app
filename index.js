var express = require('express');
var socket = require('socket.io');

// app setup
var app = express();
var server = app.listen(8000, function () {
    console.log('Server running on 8000');
});

// static files
app.use(express.static('public'));

// socket setup
var io = socket(server);

io.on('connection', function (socket) {
    var address = socket.handshake.address;
    console.log('made socket connection', address);

    socket.on('chat', function (data) {
        try{
            var data = JSON.parse(data);
        }
        catch(err) {
            console.log(err.message);
        }

        io.sockets.emit('chat', data);
    });

    socket.on('typing', function (data) {
        socket.broadcast.emit('typing', data);
    });
});





